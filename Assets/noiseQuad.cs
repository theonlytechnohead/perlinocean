﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class noiseQuad : MonoBehaviour {


	public int width = 256;
	public int height = 256;

	// Start is called before the first frame update
	void Start () {
		Renderer renderer = GetComponent<Renderer>();
		renderer.material.mainTexture = GenerateTexture();
	}

	Texture2D GenerateTexture () {
		Texture2D texture = new Texture2D(width, height);

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				Color colour = CalculateColour(x, y);
				texture.SetPixel(x, y, colour);
			}
		}
		texture.Apply();
		return texture;
	}

	Color CalculateColour (int x, int y) {
		float xCoord = (float)x / width;
		float yCoord = (float)y / height;

		float sample = Mathf.PerlinNoise(xCoord, yCoord);
		return new Color(sample, sample, sample);
	}

	// Update is called once per frame
	void Update () {

	}
}
