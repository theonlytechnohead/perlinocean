﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class noise3D : MonoBehaviour {
	public Terrain terrain;
	public TerrainData terrainData;

	float[,,] perlinData3d;

	float perlinOffset = 0f;
	[Range(0, 999)]
	public int offset = 0;
	//float materialOffset = 0f;
	float sphereRadius = Mathf.PI * 4;

	float[,,] blankAlphaMap;

	private int frames = 0;

	// Start is called before the first frame update
	void Start () {
		terrainData = terrain.terrainData;
		Application.targetFrameRate = -1;
		blankAlphaMap = new float[terrainData.alphamapWidth, terrainData.alphamapHeight, 3];
		for (int y = 0; y < terrainData.alphamapHeight; y++) {
			for (int x = 0; x < terrainData.alphamapWidth; x++) {
				blankAlphaMap[x, y, 0] = 1;
			}
		}
		perlinData3d = new float[terrainData.heightmapResolution, terrainData.heightmapResolution, 1000];
		Generate3dPerlin();
	}
	/*
	public void GenerateHeights (float tileSize, float xOffset, float yOffset) {
		float[,] heights = new float[terrainData.heightmapWidth, terrainData.heightmapHeight];

		for (int i = 0; i < terrainData.heightmapWidth; i++) {
			for (int k = 0; k < terrainData.heightmapHeight; k++) {
				//heights[i, k] = Mathf.PerlinNoise(((float)i / (float)terrain.terrainData.heightmapWidth) * tileSize + xOffset, ((float)k / (float)terrain.terrainData.heightmapHeight) * tileSize + yOffset) / 10.0f;
				heights[i, k] = 0f;
				heights[i, k] += fancyPerlin(((float)i / (float)terrainData.heightmapWidth) * tileSize, ((float)k / (float)terrainData.heightmapHeight) * tileSize, xOffset, yOffset) / 10.0f;
				//heights[i, k] += Mathf.PerlinNoise(((float)i / (float)terrain.terrainData.heightmapWidth) * tileSize * 15 + xOffset, ((float)k / (float)terrain.terrainData.heightmapHeight) * tileSize * 15 + yOffset) / 35.0f;
				//heights[i, k] = heights[i, k] / 2f;
			}
		}

		terrainData.SetHeights(0, 0, heights);
	} */

	public float fancyPerlin (float latitude, float longitude, float xOffset, float yOffset) {
		//Given longitude and latitude on a sphere of radius S, the 3D coordinates
		//P = (P.x, P.y, P.z) are:
		//P.x = S * cos(latitude) * cos(longitude)
		//P.y = S * cos(latitude) * sin(longitude)
		//P.z = S * sin(latitude)

		float x = sphereRadius * Mathf.Cos(latitude) * Mathf.Cos(longitude);
		float y = sphereRadius * Mathf.Cos(latitude) * Mathf.Sin(longitude);
		float z = sphereRadius * Mathf.Sin(latitude);
		float noise = perlin3d(x, y, z, xOffset, yOffset);
		return noise;

	}


	// TODO: Pre-create 3D perlin noise and load from cache
	public float perlin3d (float x, float y, float z, float xOffset, float yOffset) {
		x += xOffset;
		y += yOffset;

		float AB = Mathf.PerlinNoise(x, y);
		float BC = Mathf.PerlinNoise(y, z);
		float AC = Mathf.PerlinNoise(x, z);

		float BA = Mathf.PerlinNoise(y, x);
		float CB = Mathf.PerlinNoise(z, y);
		float CA = Mathf.PerlinNoise(z, x);

		float ABC = AB + BC + AC + BA + CB + CA;
		return ABC / 6f;
	}

	public void set2dPerlin (int offset) {
		float[,] heights = new float[terrainData.heightmapResolution, terrainData.heightmapResolution];

		for (int i = 0; i < terrainData.heightmapResolution; i++) {
			for (int k = 0; k < terrainData.heightmapResolution; k++) {
				heights[i, k] = perlinData3d[i, k, offset];
			}
		}

		terrainData.SetHeights(0, 0, heights);
	}

	public void Generate3dPerlin () {
		float xCoord;
		float yCoord;
		float xOffset;
		float yOffset;
		for (int i = 0; i < terrainData.heightmapResolution; i++) {
			xCoord = (float)i / terrainData.heightmapResolution;
			for (int k = 0; k < terrainData.heightmapResolution; k++) {
				yCoord = (float)k / terrainData.heightmapResolution;
				int j = 0;
				for (float circleWalk = 0f; circleWalk < Mathf.PI * 2; circleWalk += Mathf.PI * 2 / 1000f) {
					xOffset = Mathf.Sin(circleWalk);
					yOffset = Mathf.Cos(circleWalk);
					perlinData3d[i, k, j] = fancyPerlin(xCoord, yCoord, xOffset, yOffset);
					//perlinData3d[i, k, j] = Mathf.PerlinNoise(xCoord * 10 + xOffset, yCoord * 10 + yOffset) * 0.8f; // SUPER BORING
					j++;
				}
			}
		}
	}

	// Update is called once per frame
	void Update () {
		//perlinOffset += Mathf.PI * 2 / 1000f;
		if (perlinOffset >= Mathf.PI * 2) {
			//perlinOffset = 0f;
		}

		if (offset < 999) {
			offset++;
		} else {
			offset = 0;
		}

		// walk in a circle
		float xOffset = Mathf.Sin(perlinOffset);
		float yOffset = Mathf.Cos(perlinOffset);
		if (!splat) {
			terrainData.SetAlphamaps(0, 0, blankAlphaMap);
			//GenerateHeights(10f, xOffset, yOffset);
			set2dPerlin(offset);
		} else {
			frames++;
			if (frames == 0) {
				textureTerrain();
			}
			if (frames == 1) {
				//GenerateHeights(10f, xOffset, yOffset);
				set2dPerlin(offset);
				frames = -1;
			}
		}
	}

	public TerrainLayer[] terrainLayers;
	private float slopeBlendMinAngle = 90f;
	private float slopeBlendMaxAngle = 90f;

	public bool splat = true;

	public List<float> heightBlendPoints;

	public void textureTerrain () {

		if (terrain == null) return;

		terrainLayers = terrainData.terrainLayers;
		int nTextures = terrainLayers.Length;

		if (nTextures < 2) {
			Debug.LogError("Error: You must assign at least 2 textures.");
			return;
		}

		// -----------------------------------------------------------------------
		// CLIFF SLOPES REFERENCE FOR ONE HEIGHTMAP PIXEL

		int Tw = this.terrainData.heightmapResolution - 1;
		int Th = this.terrainData.heightmapResolution - 1;

		float[,] heightMapData = new float[Tw, Th];
		float[,] slopeMapData = new float[Tw, Th];
		float[,,] splatMapData = terrainData.GetAlphamaps(0, 0, Tw, Tw);

		terrainData.alphamapResolution = Tw;
		Vector3 terSize = terrainData.size;

		// At which angle cliff texture stars appearing ? >>> slopeBlendMinAngle
		// At which angle cliff texture is totally visible ? >>> slopeBlendMaxAngle
		// Convert this angle to HEIGHTMAP VALUE
		// TAN(angle) = oposite side / (adjacent side reduced to one unit !!) then normalize result (HEIGHT value !)
		// float slopeBlendMinimum = ( (terSize.x / Tw) * Mathf.Tan( slopeBlendMinAngle * Mathf.Deg2Rad ) ) / terSize.y;
		// float slopeBlendMaximum = ( (terSize.x / Tw) * Mathf.Tan( slopeBlendMaxAngle * Mathf.Deg2Rad ) ) / terSize.y;

		float angleToSlope = terSize.x / (Tw * terSize.y);
		float slopeBlendMinimum = angleToSlope * Mathf.Tan(slopeBlendMinAngle * Mathf.Deg2Rad);
		float slopeBlendMaximum = angleToSlope * Mathf.Tan(slopeBlendMaxAngle * Mathf.Deg2Rad);
		float slopeBlendAmplitude = slopeBlendMaximum - slopeBlendMinimum;

		try {

			// -----------------------------------------------------------------------
			// SLOPES PER HEIGHTMAP PIXEL NEIGHBOUR

			int xNeighbours;
			int yNeighbours;
			int xShift;
			int yShift;
			int xIndex;
			int yIndex;
			float[,] heightMap = terrainData.GetHeights(0, 0, Tw, Th);
			float h;
			float tCumulative;
			float nNeighbours;
			int Ny;
			int Nx;
			int Ty;
			int Tx;

			for (Ty = 0; Ty < Th; Ty++) {

				// y...
				if (Ty == 0) {
					yNeighbours = 2;
					yShift = 0;
					yIndex = 0;
				} else if (Ty == Th - 1) {
					yNeighbours = 2;
					yShift = -1;
					yIndex = 1;
				} else {
					yNeighbours = 3;
					yShift = -1;
					yIndex = 1;
				}

				for (Tx = 0; Tx < Tw; Tx++) {

					// x...
					if (Tx == 0) {
						xNeighbours = 2;
						xShift = 0;
						xIndex = 0;
					} else if (Tx == Tw - 1) {
						xNeighbours = 2;
						xShift = -1;
						xIndex = 1;
					} else {
						xNeighbours = 3;
						xShift = -1;
						xIndex = 1;
					}

					// Get height... And apply to height map...
					h = heightMap[Tx + xIndex + xShift, Ty + yIndex + yShift];
					heightMapData[Tx, Ty] = h;

					// Calculate average height using neighbours only (ignoring index)...
					tCumulative = 0.0f;
					nNeighbours = xNeighbours * yNeighbours - 1;
					for (Ny = 0; Ny < yNeighbours; Ny++) {
						for (Nx = 0; Nx < xNeighbours; Nx++) {
							if (Nx != xIndex || Ny != yIndex) {
								tCumulative += Mathf.Abs(h - heightMap[Tx + Nx + xShift, Ty + Ny + yShift]); // THIS is the big one?!?
							}
						}
					}

					// Compute and assign average HEIGHT value to the slope map...
					slopeMapData[Tx, Ty] = tCumulative / nNeighbours;

				}

			}

			// -----------------------------------------------------------------------

			float sBlended = 0;
			int Px;
			int Py;

			float hBlendInMinimum;
			float hBlendInMaximum;
			float hBlendOutMinimum;
			float hBlendOutMaximum;
			float hValue;
			float hBlended;
			int i;

			for (Py = 0; Py < Th; Py++) {
				for (Px = 0; Px < Tw; Px++) {

					// -----------------------------------------------------------------------
					// Blend slope... FOR CLIFF


					sBlended = slopeMapData[Px, Py];
					/*
					if (sBlended < slopeBlendMinimum) {
						sBlended = 0;
					} else if (sBlended <= slopeBlendMaximum) {
						sBlended = Mathf.Clamp01((sBlended - slopeBlendMinimum) / slopeBlendAmplitude);
					} else if (sBlended > slopeBlendMaximum) {
						sBlended = 1;
					}

					slopeMapData[Px, Py] = sBlended; */
					splatMapData[Px, Py, 0] = sBlended;
					tCumulative = sBlended;


					// -----------------------------------------------------------------------
					// Blend slope... FOR TEXTURES

					for (i = 1; i < nTextures; i++) {

						hBlendInMinimum = 0;
						hBlendInMaximum = 0;
						hBlendOutMinimum = 1;
						hBlendOutMaximum = 1;

						if (i > 1) {
							hBlendInMinimum = (float)heightBlendPoints[i * 2 - 4];
							hBlendInMaximum = (float)heightBlendPoints[i * 2 - 3];
						}

						if (i < nTextures - 1) {
							hBlendOutMinimum = (float)heightBlendPoints[i * 2 - 2];
							hBlendOutMaximum = (float)heightBlendPoints[i * 2 - 1];
						}

						hValue = heightMapData[Px, Py];
						hBlended = 0;

						if (hValue >= hBlendInMaximum && hValue <= hBlendOutMinimum) {
							// Full...
							hBlended = 1;
						} else if (hValue >= hBlendInMinimum && hValue < hBlendInMaximum) {
							// Blend in...
							hBlended = Mathf.Clamp01((hValue - hBlendInMinimum) / (hBlendInMaximum - hBlendInMinimum));
						} else if (hValue > hBlendOutMinimum && hValue <= hBlendOutMaximum) {
							// Blend out...
							hBlended = Mathf.Clamp01(1 - ((hValue - hBlendOutMinimum) / (hBlendOutMaximum - hBlendOutMinimum)));
						}

						// Assign value
						splatMapData[Px, Py, i] = Mathf.Clamp01(hBlended - slopeMapData[Px, Py]);

						// Cumulate
						tCumulative += splatMapData[Px, Py, i];

					}

					// Final test for global splatMapData impacting CLIFF BLENDING !
					if (tCumulative < 1.0f) splatMapData[Px, Py, 0] = Mathf.Clamp01(splatMapData[Px, Py, 0] + 1.0f - tCumulative);

				}

			}

			// -----------------------------------------------------------------------
			// Generate splat maps...

			// Assign generated splatmap data to terrain data
			terrainData.SetAlphamaps(0, 0, splatMapData);

			// Clean up...
			heightMapData = null;
			slopeMapData = null;
			splatMapData = null;

		} catch (System.Exception e) {

			// Clean up...
			heightMapData = null;
			slopeMapData = null;
			splatMapData = null;
			Debug.LogError("An error occurred : " + e);

		}

	}
}
