﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class oceanOrbit : MonoBehaviour {

	//public Vector3 startPositition;
	public Vector3 target;
	Vector3 pos;
	float walk;
	PostProcessVolume volume;
	Camera camera;
	public RenderingPath defaultRenderingPath;
	public RenderingPath liteRenderingPath;


	// Start is called before the first frame update
	void Start () {
		//transform.position = startPositition;
		volume = GetComponent<PostProcessVolume>();
		camera = GetComponent<Camera>();
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.P)) {
			volume.enabled = !volume.enabled;
			if (camera.renderingPath != liteRenderingPath) {
				camera.renderingPath = liteRenderingPath;
			} else {
				camera.renderingPath = defaultRenderingPath;
			}
		}
		transform.LookAt(target);
		walk += Mathf.PI * 2 / 5000f;
		pos = transform.position;
		pos.x = Mathf.Sin(walk) * 500 + 500;
		pos.z = Mathf.Cos(walk) * 500 + 500;
		transform.position = pos;
		//transform.Translate(Vector3.left * target.x * Mathf.PI * 2 / 1000f);
	}
}
