﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class noise2D : MonoBehaviour {
	public Terrain terrain;

	public float perlinOffset = 0.1f;

	// Start is called before the first frame update
	void Start () {

	}

	public void GenerateHeights (Terrain terrain, float tileSize, float xOffset, float yOffset) {
		float[,] heights = new float[terrain.terrainData.heightmapResolution, terrain.terrainData.heightmapResolution];

		for (int i = 0; i < terrain.terrainData.heightmapResolution; i++) {
			for (int k = 0; k < terrain.terrainData.heightmapResolution; k++) {
				heights[i, k] = Mathf.PerlinNoise(((float)i / (float)terrain.terrainData.heightmapResolution) * tileSize + xOffset, ((float)k / (float)terrain.terrainData.heightmapResolution) * tileSize + yOffset) / 10.0f;
			}
		}

		terrain.terrainData.SetHeights(0, 0, heights);
	}

	// Update is called once per frame
	void Update () {
		GenerateHeights(terrain, 20f, Mathf.Sin(perlinOffset), Mathf.Cos(perlinOffset));
		perlinOffset += 0.01f;
	}
}
