﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class heightSplatter : MonoBehaviour {
	public TerrainLayer[] terrainLayers;
	private float slopeBlendMinAngle = 90f;
	private float slopeBlendMaxAngle = 90f;

	private int frames = 0;

	public List<float> heightBlendPoints;

	// Start is called before the first frame update
	void Start () {
		//textureTerrain();
	}

	// Update is called once per frame
	void Update () {
		frames++;
		if (frames == 1) {
			textureTerrain();
		}
		if (frames == 2) {
			frames = 0;
		}
		//textureTerrain();
	}

	public void textureTerrain () {

		Terrain ter = (Terrain)GetComponent(typeof(Terrain));
		if (ter == null) return;

		TerrainData terData = ter.terrainData;
		terrainLayers = terData.terrainLayers;
		int nTextures = terrainLayers.Length;

		if (nTextures < 2) {
			Debug.LogError("Error: You must assign at least 2 textures.");
			return;
		}

		// -----------------------------------------------------------------------
		// CLIFF SLOPES REFERENCE FOR ONE HEIGHTMAP PIXEL

		int Tw = terData.heightmapResolution - 1;
		int Th = terData.heightmapResolution - 1;

		float[,] heightMapData = new float[Tw, Th];
		float[,] slopeMapData = new float[Tw, Th];
		float[,,] splatMapData = terData.GetAlphamaps(0, 0, Tw, Tw);

		terData.alphamapResolution = Tw;
		Vector3 terSize = terData.size;

		// At which angle cliff texture stars appearing ? >>> slopeBlendMinAngle
		// At which angle cliff texture is totally visible ? >>> slopeBlendMaxAngle
		// Convert this angle to HEIGHTMAP VALUE
		// TAN(angle) = oposite side / (adjacent side reduced to one unit !!) then normalize result (HEIGHT value !)
		// float slopeBlendMinimum = ( (terSize.x / Tw) * Mathf.Tan( slopeBlendMinAngle * Mathf.Deg2Rad ) ) / terSize.y;
		// float slopeBlendMaximum = ( (terSize.x / Tw) * Mathf.Tan( slopeBlendMaxAngle * Mathf.Deg2Rad ) ) / terSize.y;

		float angleToSlope = terSize.x / (Tw * terSize.y);
		float slopeBlendMinimum = angleToSlope * Mathf.Tan(slopeBlendMinAngle * Mathf.Deg2Rad);
		float slopeBlendMaximum = angleToSlope * Mathf.Tan(slopeBlendMaxAngle * Mathf.Deg2Rad);
		float slopeBlendAmplitude = slopeBlendMaximum - slopeBlendMinimum;

		try {

			// -----------------------------------------------------------------------
			// SLOPES PER HEIGHTMAP PIXEL NEIGHBOUR

			int xNeighbours;
			int yNeighbours;
			int xShift;
			int yShift;
			int xIndex;
			int yIndex;
			float[,] heightMap = terData.GetHeights(0, 0, Tw, Th);
			float h;
			float tCumulative;
			float nNeighbours;
			int Ny;
			int Nx;
			int Ty;
			int Tx;

			for (Ty = 0; Ty < Th; Ty++) {

				// y...
				if (Ty == 0) {
					yNeighbours = 2;
					yShift = 0;
					yIndex = 0;
				} else if (Ty == Th - 1) {
					yNeighbours = 2;
					yShift = -1;
					yIndex = 1;
				} else {
					yNeighbours = 3;
					yShift = -1;
					yIndex = 1;
				}

				for (Tx = 0; Tx < Tw; Tx++) {

					// x...
					if (Tx == 0) {
						xNeighbours = 2;
						xShift = 0;
						xIndex = 0;
					} else if (Tx == Tw - 1) {
						xNeighbours = 2;
						xShift = -1;
						xIndex = 1;
					} else {
						xNeighbours = 3;
						xShift = -1;
						xIndex = 1;
					}

					// Get height... And apply to height map...
					h = heightMap[Tx + xIndex + xShift, Ty + yIndex + yShift];
					heightMapData[Tx, Ty] = h;

					// Calculate average height using neighbours only (ignoring index)...
					tCumulative = 0.0f;
					nNeighbours = xNeighbours * yNeighbours - 1;
					for (Ny = 0; Ny < yNeighbours; Ny++) {
						for (Nx = 0; Nx < xNeighbours; Nx++) {
							if (Nx != xIndex || Ny != yIndex) {
								tCumulative += Mathf.Abs(h - heightMap[Tx + Nx + xShift, Ty + Ny + yShift]);
							}
						}
					}

					// Compute and assign average HEIGHT value to the slope map...
					slopeMapData[Tx, Ty] = tCumulative / nNeighbours;

				}

			}

			// -----------------------------------------------------------------------

			float sBlended = 0;
			int Px;
			int Py;

			float hBlendInMinimum;
			float hBlendInMaximum;
			float hBlendOutMinimum;
			float hBlendOutMaximum;
			float hValue;
			float hBlended;
			int i;

			for (Py = 0; Py < Th; Py++) {
				for (Px = 0; Px < Tw; Px++) {

					// -----------------------------------------------------------------------
					// Blend slope... FOR CLIFF


					sBlended = slopeMapData[Px, Py];
					/*
					if (sBlended < slopeBlendMinimum) {
						sBlended = 0;
					} else if (sBlended <= slopeBlendMaximum) {
						sBlended = Mathf.Clamp01((sBlended - slopeBlendMinimum) / slopeBlendAmplitude);
					} else if (sBlended > slopeBlendMaximum) {
						sBlended = 1;
					}

					slopeMapData[Px, Py] = sBlended; */
					splatMapData[Px, Py, 0] = sBlended;
					tCumulative = sBlended;


					// -----------------------------------------------------------------------
					// Blend slope... FOR TEXTURES

					for (i = 1; i < nTextures; i++) {

						hBlendInMinimum = 0;
						hBlendInMaximum = 0;
						hBlendOutMinimum = 1;
						hBlendOutMaximum = 1;

						if (i > 1) {
							hBlendInMinimum = (float)heightBlendPoints[i * 2 - 4];
							hBlendInMaximum = (float)heightBlendPoints[i * 2 - 3];
						}

						if (i < nTextures - 1) {
							hBlendOutMinimum = (float)heightBlendPoints[i * 2 - 2];
							hBlendOutMaximum = (float)heightBlendPoints[i * 2 - 1];
						}

						hValue = heightMapData[Px, Py];
						hBlended = 0;

						if (hValue >= hBlendInMaximum && hValue <= hBlendOutMinimum) {
							// Full...
							hBlended = 1;
						} else if (hValue >= hBlendInMinimum && hValue < hBlendInMaximum) {
							// Blend in...
							hBlended = Mathf.Clamp01((hValue - hBlendInMinimum) / (hBlendInMaximum - hBlendInMinimum));
						} else if (hValue > hBlendOutMinimum && hValue <= hBlendOutMaximum) {
							// Blend out...
							hBlended = Mathf.Clamp01(1 - ((hValue - hBlendOutMinimum) / (hBlendOutMaximum - hBlendOutMinimum)));
						}

						// Assign value
						splatMapData[Px, Py, i] = Mathf.Clamp01(hBlended - slopeMapData[Px, Py]);

						// Cumulate
						tCumulative += splatMapData[Px, Py, i];

					}

					// Final test for global splatMapData impacting CLIFF BLENDING !
					if (tCumulative < 1.0f) splatMapData[Px, Py, 0] = Mathf.Clamp01(splatMapData[Px, Py, 0] + 1.0f - tCumulative);

				}

			}

			// -----------------------------------------------------------------------
			// Generate splat maps...

			// Assign generated splatmap data to terrain data
			terData.SetAlphamaps(0, 0, splatMapData);

			// Clean up...
			heightMapData = null;
			slopeMapData = null;
			splatMapData = null;

		} catch (System.Exception e) {

			// Clean up...
			heightMapData = null;
			slopeMapData = null;
			splatMapData = null;
			Debug.LogError("An error occurred : " + e);

		}

	}
}
